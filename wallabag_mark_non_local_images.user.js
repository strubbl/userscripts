// ==UserScript==
// @author        Strubbl
// @name          Wallabag mark non-local images
// @description   Adds a red border to images in an article, which are not from the defined wallabag server. You have to adjust the following include line to make this user script work with your wallabag instance
// @include       https://wallabag.linux4tw.de/view/*
// @grant         none
// @version       1.3
// @updateURL     https://codeberg.org/strubbl/userscripts/raw/branch/master/wallabag_mark_non_local_images.user.js
// ==/UserScript==


var local_img_src = "^/assets/images/";
var warning_text_pre = "<p style='color: red;'>"
var warning_text = "external images found.</p>"

$(document).ready(function() {
	var foundExternalImage = false;
	var numFoundExternalImage = 0;
	$("img").each(function() {
		if(!$(this).attr("src").match(local_img_src)) {
			console.log("found non-local image source: ", $(this).attr("src"));
			$(this).css('border', '3px solid red');
			foundExternalImage = true;
			numFoundExternalImage++;
		}
	});
	if(foundExternalImage) {
		warning_text = warning_text_pre + numFoundExternalImage + " " + warning_text
		$("#content").append(warning_text);
		$("#content").prepend(warning_text);
	}
});

