// ==UserScript==
// @author       Strubbl
// @name         OpenStreetMap Sidebar Resizer
// @description  makes the sidebar resizable, see the bottom right of the sidebar to resize it horizontally
// @match        https://www.openstreetmap.org/*
// @grant        none
// @updateURL    https://codeberg.org/strubbl/userscripts/raw/branch/master/osm_sidebar_resizer.user.js
// @version      1.2
// ==/UserScript==

function addGlobalStyle(css) {
    var head, style;
    head = document.getElementsByTagName('head')[0];
    if (!head) { return; }
    style = document.createElement('style');
    style.type = 'text/css';
    style.innerHTML = css;
    head.appendChild(style);
}

addGlobalStyle('#sidebar { resize: horizontal; overflow: auto; } @media (max-width: 767.98px){ #sidebar { resize: none; } }');

