# User scripts

This repo contains some [Greasemonkey](http://www.greasespot.net/) user scripts.

* **diaspora_hide_reshares.user.js**: Hides all posts in the Diaspora* stream, which are reposts
* **osm_changeset_analyze_urls.user.js**: Add links to achavi, achavi dev and osmcha to every changeset page
* **osm_nwr_history_pewu.user.js**: Add a link to the History Viewer from PeWu to every node, way and relation page
* **osm_sidebar_resizer.user.js**: Makes the changeset sidebar, where the comment fields are horizontally resizable
* **postfixadmin_alias_prefill.user.js**: Prefill the mail alias from of Postfixadmin with a given mail adress, script needs to be adopted for your desired mail
* **reddit_hide_all.user.js**: Automatically clicks all the hide buttons on the current [Reddit](https://www.reddit.com/) page
* **wallabag_mark_non_local_images.user.js**:

